package hellopackage;
import java.util.Scanner;
import secondpackage.Utilities;
import java.util.Random;

public class Greeter{
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();

        System.out.println("Enter an integer  value.");
        int num = scan.nextInt();

        Utilities method = new Utilities();
        method.doubleMe(num);
    }
}